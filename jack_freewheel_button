#!/usr/bin/python

# JACK FreeWheel button
#
# Copyright (C) 2010 Nikita Zlobin <cook60020tmp@mail.ru>
#
#*************************************************************************
# This file implements dialogs that are not implemented in dedicated files
#*************************************************************************
#
# LADI Session Handler is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# LADI Session Handler is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LADI Session Handler. If not, see <http://www.gnu.org/licenses/>
# or write to the Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.


import glib
import gtk
import jack

fw_on = "<b>ON</b>"
fw_off = "<b>OFF</b>"
fw_slave = "<b>ON</b> <small>SLAVE</small>"
text_generic = "<small>FREE WHEEL</small>"

jclient_name = "jack_freewheel_button"

def on_button_toggled(button):
  label = button.get_children()[0]
  if button.get_active():
    b_label.set_label(text_generic+"\n"+fw_on)
    jack.set_freewheel(1)

    global blink_state
    blink_state = True
    button.modify_bg(gtk.STATE_NORMAL, backlight_bg)
    blink_toggle()
  else:
    button.modify_bg(gtk.STATE_NORMAL, normal_bg)
    b_label.modify_fg(gtk.STATE_NORMAL, normal_fg)
    b_label.set_label(text_generic+"\n"+fw_off)

    button.set_sensitive(True)
    jack.set_freewheel(0)

def check_freewheel():
  if jack.get_freewheel() > 0:
    if button.get_active() == False:
      button.set_active(True)
      button.set_sensitive(False)
      b_label.set_label("<span foreground=\"darkorange\">"+text_generic+"\n"+fw_slave+"</span>")
  else:
    button.set_active(False)
    button.set_sensitive(True)

  return True

def blink_toggle():
  if not button.get_active():
    return True

  global blink_state
  if blink_state:
    button.modify_bg(gtk.STATE_ACTIVE, normal_bg)
    button.modify_bg(gtk.STATE_PRELIGHT, normal_bg)
    b_label.modify_fg(gtk.STATE_NORMAL, normal_fg)
    b_label.modify_fg(gtk.STATE_ACTIVE, normal_fg)
    blink_state = False
  else:
    button.modify_bg(gtk.STATE_ACTIVE, backlight_bg)
    button.modify_bg(gtk.STATE_PRELIGHT, backlight_bg)
    b_label.modify_fg(gtk.STATE_NORMAL, backlight_fg)
    b_label.modify_fg(gtk.STATE_ACTIVE, backlight_fg)
    blink_state = True
  return True

# Create objects
window = gtk.Window()
window.set_title("JACK time wheel control")
window.set_resizable(False)
window.set_decorated(False)

button = gtk.ToggleButton()
button.set_active(False)
button.set_label(text_generic+"\n"+fw_off)

b_label = button.get_children()[0]
b_label.set_justify(gtk.JUSTIFY_CENTER)
b_label.set_use_markup(True)

blink_state = False

# Connections
window.connect("delete-event", gtk.main_quit);
button.connect("toggled", on_button_toggled);
window.add(button)

# Colors
b_style = button.get_modifier_style().copy()
t_style = b_label.get_modifier_style().copy()
normal_bg = b_style.bg[gtk.STATE_ACTIVE]
normal_fg = t_style.fg[gtk.STATE_ACTIVE]
backlight_bg = gtk.gdk.color_parse('brown')
backlight_fg = gtk.gdk.color_parse('darkorange')
button.modify_bg(gtk.STATE_INSENSITIVE, backlight_bg)

# JACK
jack.attach(jclient_name)
jack.activate()
glib.timeout_add(300, check_freewheel)
glib.timeout_add(700, blink_toggle)

window.show_all()

gtk.main()

jack.deactivate()
jack.detach()
